  require([
      "esri/Map",
      "esri/views/MapView",
      "esri/layers/FeatureLayer"
    ], function(Map, MapView, FeatureLayer) {

    var map = new Map({
      basemap: "topo"
    });

    var view = new MapView({
      container: "viewDiv",
      map: map,
      center: [-110.658510, 31.890920], // longitude, latitude of my home
      zoom: 10
    });
    
    var trailheadsLabels = {
      symbol: {
        type: "text",
        color: "#FFFFFF",
        haloColor: "#5E8D74",
        haloSize: "2px",
        font: {
          size: "12px",
          family: "Noto Sans",
          style: "italic",
          weight: "normal"
        }
      },
      labelPlacement: "above-center",
      labelExpressionInfo: {
        expression: "$feature.TrailName"
      }
    };
    
    //trailsRenderer defines the look of the trails on the map
    var trailsRenderer = {
        type: "simple",
        symbol: {
            color: "#BA55D3",
            type: "simple-line",
            style: "solid"
        },
        visualVariables: [{
            type: "size",
            field: "Miles",
            minDataValue: 0,
            maxDataValue: 50,
            minSize: "3px",
            maxSize: "7px"
        }]
    };
    
    //new feature layer with trails data
    var trails = new FeatureLayer ({
      url:  "https://services2.arcgis.com/gdcQ6sUWKP8qwBmV/arcgis/rest/services/Trails_of_Arizona/FeatureServer/0",
      renderer: trailsRenderer,
      labelingInfo: [trailheadsLabels],
      opacity: 0.75,

      outFields: ["TrailName", "Miles", "Website"],

      //popup that contains trail data
      popupTemplate: {
          title: "{TrailName}",
          content: "The trail is {Miles} miles long.If more info is available, \n\
                      it can be found here: {Website}."
          }
        });

    //add feature layer to the map
    map.add(trails);
        
  });